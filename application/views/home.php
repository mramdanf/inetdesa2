<!-- slider-section-start -->
<section class="slider_area">
	<div id="image_carousel_1" class="carousel slide" data-ride="carousel">
		<ol class="carousel-indicators slider_indicators">
			<li data-target="#image_carousel_1" data-slide-to="0" class="active"></li>
			<li data-target="#image_carousel_1" data-slide-to="1"></li>
			<li data-target="#image_carousel_1" data-slide-to="2"></li>
		</ol>
		<div class="carousel-inner" role="listbox">
			<div class="item active">
				<div class="single_slide">
					<div class="slider_bg slide-2" style="background-image:url('./assets/images/slider/2.jpg');"></div>
					<div class="container">
						<div class="row">
							<div class="col-md-4 col-sm-6">
								<div class="slide_text">
									<div class="table">
										<div class="table-cell" data-animation="animated slideInUp">
											<h1>Internet Desa</h1>
											<div class="bg-yellow"></div>
											<span>Pemerataan teknologi ke desa - desa 3T dengan teknolog jaringan satelit yang handal.</span><br />
											
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="item">
				<div class="single_slide">
					<div class="slider_bg slide-1" style="background-image:url('./assets/images/slider/1.jpg');"></div>
					<div class="container">
						<div class="row">
							<div class="col-md-4 col-sm-6">
								<div class="slide_text">
									<div class="table">
										<div class="table-cell" data-animation="animated slideInUp">
											<h1>Desa Broadband Terpadu</h1>
											<div class="bg-yellow"></div>
											<span>Kami turut serta dalam membangun infrastruktur jaringan dan akses internet untuk kabupaten/kota Lokasi Prioritas (LOKPRI) .</span><br />
											
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="item">
				<div class="single_slide">
					<div class="slider_bg slide-3" style="background-image:url('./assets/images/slider/3.jpg');"></div>
					<div class="container">
						<div class="row">
							<div class="col-md-4 col-sm-6">
								<div class="slide_text">
									<div class="table">
										<div class="table-cell" data-animation="animated slideInUp">
											<h1>Akses Internet (AI)</h1>
											<div class="bg-yellow"></div>
											<span>Kami turut serta dalam membangun infrastruktur jaringan pada sekolah-sekolah, puskesmas, ataupun lembaga kerja.</span><br />
											
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<a class="left fawesome-control" href="#image_carousel_1" role="button" data-slide="prev"><img src="assets/images/slider/arrow-pre.png" alt="" /></a>
		<a class="right fawesome-control" href="#image_carousel_1" role="button" data-slide="next"><img src="assets/images/slider/arrow-next.png" alt="" /></a>
	</div>
</section>
<!-- slider-section-end -->

<!-- About-section-start -->
<section class="about section_padding" id="about">
	<div class="container">
		<div class="row text-center">
			<div class="about_title col-text-center section-title wow flipInX" data-wow-delay="0.3s">
				<h1>TENTANG INETDESA</h1>
				<p>Dalam rangka pengembangan potensi desa melalui
				pemanfaatan teknologi infomasi dan komunikasi,<br>
				Kementerian Komunikasi dan Informatika (Kemenkominfo)
				mengembangkan program pembangunan desa broadband
				terpadu.<br> Desa broadband terpadu adalah desa yang akan
				dilengkapi dengan fasilitas jaringan atau akses internet,<br>
				perangkat akhir pengguna dan aplikasi yang sesuai dengan
				karakteristik penduduk setempat. <br>Progam tersebut
				diperuntukkan pada desa nelayan, desa pertanian, <br>dan desa
				pedalaman dalam rangka mendukung dan membantu
				kegiatan masyarakat setempat sehari-hari.
				</p>
			</div>
			<div class="col-sm-3">
				<div class="about_text wow fadeInLeft" data-wow-delay="0.3s">
					<div class="number_text">
						<h1 class="yellow_number">47</h1>
						<h1 class="black_number">47</h1>
					</div>
					<span class="black_text">Desa Broadband <br />Terpadu (DBBT)</span>
					<p>Proyek desa
					broadband adalah <br> pengadaan infrastruktur
					jaringan pada desa</p>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="about_text wow fadeInUp" data-wow-delay="0.3s">
					<div class="number_text">
						<h1 class="yellow_number">238</h1>
						<h1 class="black_number">238</h1>
					</div>
					<span class="black_text">Akses Internet (AI)</span>
					<p>Pengadaan infrastruktur
						jaringan pada sekolah-sekolah, puskesmas,
						ataupun lembaga kerja.</p>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="about_text wow fadeInRight" data-wow-delay="0.3s">
					<div class="number_text">
						<h1 class="yellow_number">24</h1>
						<h1 class="black_number">24</h1>
					</div>
					<span class="black_text">BTS Blankspot BP3TI</span>
					<p>Pengadaan BTS pada daerah - daerah terpencil (blankspot), sebagai bagian dari program Universal Service Obligation (USO).</p>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="about_text wow fadeInRight" data-wow-delay="0.3s">
					<div class="number_text">
						<h1 class="yellow_number">230</h1>
						<h1 class="black_number">230</h1>
					</div>
					<span class="black_text">Mangoesky Desa</span>
					<p>Layanan mangoeksy untuk daerah Tertinggal, Terluar, dan Terpencil (3T).</p>
				</div>
			</div>
		</div>
		<hr class="margin_eight">
	</div>
</section>
<!-- about-section-end -->

<!-- news-section-start -->
<section class="what_do section_padding text-center">
	<div class="container">
		<div class="row">
			<div class="what_do_title col-text-center wow flipInX animated" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: flipInX;">
				<p>Kami bersama pemerintah mewujudkan pemerataan akses internet ke seluruh penjuru nusantara</p>
			</div>
		</div>
	</div>
</section>
<!-- news-section-end -->

<!-- work-section-start -->
<section class="how_to_work section_padding gallery" id="work">
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-sm-6 col-text-center text-center">
				<div class="work_text section-title wow flipInX" data-wow-delay="0.3s">
					<h1>BAGAIMANA KAMI BEKERJA</h1>
					<p>Loyalitas terhadap pelanggan merupakan salah satu komitment kami.</p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8 col-sm-10 col-text-center">
				<div class="menu_item wow flipInX" data-wow-delay="0.3s">
					<ul>
						<li data-filter="*" class="active">SEMUA</li>
						<li data-filter=".image">FOTO</li>
						<li data-filter=".video">VIDEO</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="gallery_item">
		<?php 
		$gallery_type = "";
		$helperId = "";
		foreach ($galleries as $gallery) { 
			if ($gallery->type == "1") {
				$gallery_type = "image";
				$helperId = $gallery->item_name;
			} else {
				$gallery_type = "video";
				$helperId = $gallery->y_id;
			}
			?>
			<div class="single_items <?=$gallery_type?> launch_modal_img" id="<?=$helperId?>" data-toggle="modal"  data-target="gallery-modal">
				<img src="assets/images/<?=$gallery->item_name?>"/>
				<div class="popup_text">
					<p><?=$gallery->description?></p>
					<span><?=$gallery->title?></span>
				</div>
			</div>
		<?php } ?>
	</div>
	<div class="container text-center">
		<div class="center-btn-wrapper">
			<a href="<?=site_url('gallery#work')?>" class="white-btn">VIEW MORE</a>
		</div>		
	</div>
	<div class="modal fade" id="gallery-modal" tabindex="-1" role="dialog" style="display: none;">
	<div class="modal-dialog" role="document">
    	<div class="modal-content">
      		<div class="modal-header">
	        	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
	      	</div>
			<div class="modal-body">
				<img id="img_modal" src="" alt="" />
				<div class="embed-responsive embed-responsive-16by9 vid-wrapp" style="display: none;"> 
                    <iframe id="vi-iframe" class="embed-responsive-item" src="https://www.youtube.com/embed/K44VUjxJhWs"></iframe> 
                </div>
			</div>
    	</div>
  	</div>
</div>
</section>
<!-- work-section-end -->

<aside class="partner">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-6">
                <a href="https://www.kominfo.go.id/" target="_blank">
                    <img src="assets/images/partner/kominfo.png" class="img-responsive img-centered" alt="">
                </a>
            </div>
            <div class="col-md-3 col-sm-6">
                <a href="http://www.bp3ti.kominfo.go.id/" target="_blank">
                    <img src="assets/images/partner/bp3ti.png" class="img-responsive img-centered" alt="">
                </a>
            </div>
            <div class="col-md-3 col-sm-6">
                <a href="http://www.telkom.co.id/" target="_blank">
                    <img src="assets/images/partner/telkom.png" class="img-responsive img-centered" alt="">
                </a>
            </div>
            <div class="col-md-3 col-sm-6">
                <a href="http://www.metrasat.co.id" target="_blank">
                    <img src="assets/images/partner/metrasat.png" class="img-responsive img-centered" alt="">
                </a>
            </div>
        </div>
    </div>
</aside>