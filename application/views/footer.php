	<!-- footer-section-start -->
	<footer>
		<div class="container">
			<div class="row">
				<div class="footer_copyright">
					<p>&copy;2017 Inetdesa. Theme by <a href="http://devitems.com/author/rakib9382/">rakib</a></p>
					<a href="#"><img src="assets/images/logo.png" alt="" /></a>
				</div>
			</div>
		</div>
	</footer>
	<!-- footer-section-end -->
	<!-- jquery-1.11.3 -->
    <script src="<?=site_url('assets/js/jquery-1.11.3.min.js')?>"></script>
	<!-- bootstrap js -->
    <script src="<?=site_url('assets/js/bootstrap.min.js')?>"></script>
	<!-- slicknav js -->
    <script src="<?=site_url('assets/js/jquery.slicknav.min.js')?>"></script>
	<!-- easyPieChart -->
    <script src="<?=site_url('assets/js/easyPieChart.js')?>"></script>
	<!-- sticky -->
	<script src="<?=site_url('assets/js/jquery.sticky.js')?>"></script>
	<!-- scrollUp -->
	<script src="<?=site_url('assets/js/jquery.scrollUp.js')?>"></script>
	<!-- counterup -->
    <script src="<?=site_url('assets/js/counterup.min.js')?>"></script>
	<script src="<?=site_url('assets/js/waypoints.min.js')?>"></script>
	<!-- wow -->
    <script src="<?=site_url('assets/js/wow.min.js')?>"></script>
	<!-- isotope -->
	<script src="<?=site_url('assets/js/isotope.pkgd.min.js')?>"></script>
	<!-- owl-carousel -->
	<script src="<?=site_url('assets/js/owl.carousel.min.js')?>"></script>
	<!-- main js -->
    <script src="<?=site_url('assets/js/main.js')?>"></script>
  </body>
</html>