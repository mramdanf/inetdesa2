<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Inetdesa | <?=$title?></title>
	<!-- fonts -->
	<link href='https://fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>
	<!-- bootstrap -->
	<link rel="stylesheet" href="<?=site_url('assets/css/bootstrap.min.css')?>" />
	<!-- fontawesome -->
	<link rel="stylesheet" href="<?=site_url('assets/css/font-awesome.min.css')?>" />
	<!-- animate -->
    <link  rel="stylesheet" type="text/css" href="<?=site_url('assets/css/animate.min.css')?>"/>
	<!-- owl-carousel -->
	<link rel="stylesheet" href="<?=site_url('assets/css/owl.carousel.css')?>" />
	<!-- slicknav -->
    <link rel="stylesheet" href="<?=site_url('assets/css/slicknav.css')?>">
	<!-- style css -->
    <link rel="stylesheet" href="<?=site_url('assets/css/style.css')?>">
	<!-- responsive -->
    <link rel="stylesheet" href="<?=site_url('assets/css/responsive.css')?>"/>
	<!-- favicon -->
    <link rel="shortcut icon" href="<?=site_url('assets/images/icon.ico')?>" />
	
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body data-spy="scroll" data-target="#navbar-example">
	<div id="home"></div>
	<!-- header-section-start -->
    <header>
		<div class="container">
			<div class="row">
				<div class="logo">
					<a href="<?=site_url('home')?>"><img src="assets/images/logo.png" alt="" /></a>
				</div>
				<nav class="mainmenu"  id="navbar-example">
					<ul id="nav" class="nav nav-tabs" role="tablist">
						<?=$navigation?>
					</ul>
				</nav>
			</div>
		</div>
	</header>