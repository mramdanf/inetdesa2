<!-- slider-section-start -->
<section class="gallery_head">
	<div class="gallery_head_bg"></div>
</section>
<section class="gallery_content section_padding" id="work">
	
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-sm-6 col-text-center text-center">
				<div class="work_text section-title wow flipInX" data-wow-delay="0.3s">
					<h1>BAGAIMANA KAMI BEKERJA</h1>
					<p>Loyalitas terhadap pelanggan merupakan salah satu komitment kami.</p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8 col-sm-10 col-text-center">
				<div class="menu_item wow flipInX" data-wow-delay="0.3s">
					<ul>
						<li data-filter="*" class="active">SEMUA</li>
						<li data-filter=".image">FOTO</li>
						<li data-filter=".video">VIDEO</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="gallery_item">
		<?php 
		$gallery_type = "";
		$helperId = "";
		foreach ($galleries as $gallery) { 
			if ($gallery->type == "1") {
				$gallery_type = "image";
				$helperId = $gallery->item_name;
			} else {
				$gallery_type = "video";
				$helperId = $gallery->y_id;
			}
			?>
			<div class="single_items <?=$gallery_type?> launch_modal_img" id="<?=$helperId?>" data-toggle="modal"  data-target="gallery-modal">
				<img src="assets/images/<?=$gallery->item_name?>"/>
				<div class="popup_text">
					<p><?=$gallery->description?></p>
					<span><?=$gallery->title?></span>
				</div>
			</div>
		<?php } ?>
	</div>
	<div class="modal fade" id="gallery-modal" tabindex="-1" role="dialog" style="display: none;">
	<div class="modal-dialog" role="document">
    	<div class="modal-content">
      		<div class="modal-header">
	        	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
	      	</div>
			<div class="modal-body">
				<img id="img_modal" src="" alt="" />
				<div class="embed-responsive embed-responsive-16by9 vid-wrapp" style="display: none;"> 
                    <iframe id="vi-iframe" class="embed-responsive-item" src="https://www.youtube.com/embed/"></iframe> 
                </div>
			</div>
    	</div>
  	</div>
	
</section>
<!-- slider-section-end -->