<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Admin | Login</title>

<link href="<?=site_url('assets/lumino/css/bootstrap.min.css')?>" rel="stylesheet">
<link href="<?=site_url('assets/lumino/css/styles.css')?>" rel="stylesheet">

<!--[if lt IE 9]>
<script src="js/html5shiv.js"></script>
<script src="js/respond.min.js"></script>
<![endif]-->

<!--Icons-->
<script src="<?=site_url('assets/lumino/js/lumino.glyphs.js')?>"></script>

</head>

<body>
	
	<div class="row">
		<div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
			<?php $alert_msg = $this->session->flashdata(utility::ALERT_MSG); ?>
			<?php if($alert_msg != NULL) { ?>
			<div class="alert bg-danger alert-dismissable" role="alert">
				<svg class="glyph stroked cancel"><use xlink:href="#stroked-cancel"></use></svg> 
				<?=$alert_msg['msg']?> 
				<a href="#" class="pull-right" data-dismiss="alert"><span class="glyphicon glyphicon-remove"></span></a>
			</div>
			<?php } ?>
			<div class="login-panel panel panel-default">
				<div class="panel-heading">Inetdesa Admin</div>
				<div class="panel-body">
					<form action="<?=site_url('admin/auth/check_user')?>" method="post">
						<fieldset>
							<div class="form-group">
								<input class="form-control" placeholder="Username" name="username" autofocus required>
							</div>
							<div class="form-group">
								<input class="form-control" placeholder="Password" name="password" type="password" required>
							</div>
							<div class="checkbox">
								<label>
									<input name="remember" type="checkbox" value="Remember Me">Remember Me
								</label>
							</div>
							<button type="submit" class="btn btn-primary">Login</button>
						</fieldset>
					</form>
				</div>
			</div>
		</div><!-- /.col-->
	</div><!-- /.row -->	
	
		

	<script src="<?=site_url('assets/lumino/js/jquery-1.11.1.min.js')?>"></script>
	<script src="<?=site_url('assets/lumino/js/bootstrap.min.js')?>"></script>
</body>

</html>
