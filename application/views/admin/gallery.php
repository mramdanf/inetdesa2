<div class="row">
	<div class="col-lg-12">
		<?php $alert = $this->session->flashdata(utility::ALERT_MSG); ?>
		<?php if($alert != NULL) { ?>
		<div class="alert <?=$alert['type']?> alert-dismissable" role="alert">
			<svg class="<?=$alert['icon']?>"><use xlink:href="#<?=$alert['icon2']?>"></use></svg> 
			<?=$alert['msg']?> 
			<a href="#" class="pull-right" data-dismiss="alert"><span class="glyphicon glyphicon-remove"></span></a>
		</div>
		<?php } ?>
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3>Form Upload Foto dan Video</h3>
			</div>
			<div class="panel-body tabs">
					
				<ul class="nav nav-pills">
					<li class="active"><a href="#fototab" data-toggle="tab">Foto</a></li>
					<li><a href="#videotab" data-toggle="tab">Video</a></li>
				</ul>

				<div class="tab-content">
					<div class="tab-pane fade active in" id="fototab">
						<ul>
							<li><small><i>* Mohon ukuran image tidak lebih dari 1 MB. Image bisa dikompres di <a href="https://tinypng.com/" target="_blank">tinypng</a></i></small></li>
							<li><small><i>* Mohon gunakan image dengan tipe JPG, JPEG atau PNG.</small></i></li>
						</ul>						

						<form role="form" action="<?=site_url('admin/gallery/upload')?>" method="post" enctype="multipart/form-data">
							
							<!-- hidden input for help -->
							<input type="hidden" name="type" value="1">

							<div class="col-md-6">
								<div class="form-group">
									<label>Pilih File</label>
									<input type="file" class="form-control" name="img_file" required="" >
								</div>
								<div class="form-group">
									<label>Judul Gambar</label>
									<input class="form-control" name="title">
								</div>
								<div class="form-group">
									<label>Deskripsi Singkat</label>
									<input class="form-control" name="description">
								</div>
								<div class="form-group">
									<input type="submit" value="Submit" class="btn btn-primary">				
								</div>					
							</div>				
						
						</form>
					</div>
					<div class="tab-pane fade" id="videotab">
						<form role="form" action="<?=site_url('admin/gallery/upload')?>" method="post">

							<!-- hidden input for help -->
							<input type="hidden" name="type" value="2">
					
							<div class="col-md-6">
								<div class="form-group">
									<label>Link Youtube Video</label>
									<input type="text" class="form-control" name="y_id" required>
								</div>
								<div class="form-group">
									<label>Judul Video</label>
									<input class="form-control" name="title">
								</div>
								<div class="form-group">
									<label>Deskripsi Singkat</label>
									<input class="form-control" name="description">
								</div>
								<div class="form-group">
									<input type="submit" value="Submit" class="btn btn-primary">				
								</div>			
							</div>				
						
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">List Foto dan Video</div>
			<div class="panel-body">
				<form class="form-inline additonal-block">
					<input type="radio" name="filter" value="foto"> Foto
					<input type="radio" name="filter" value="video"> Video
				</form>
				<?php if ($type == "1") { ?>
				<ul class="first">
					<?php foreach ($galleries as $gallery) { ?>
				    <li>
				        <img alt="<?=$gallery->title?>" src="../assets/images/<?=$gallery->item_name?>">
				        <div class="text"><?=$gallery->description?></div>
				    </li>
				    <?php } ?>
				</ul>
				<?php } ?>
				<?php if ($type == "2") {  ?>
				<ul class="second">
					<?php foreach ($galleries as $gallery) { ?>
				    <li>
				        <a href="#" id="<?=$gallery->y_id?>" data-modal-id="modal-video" class="launch-modal"><img alt="<?=$gallery->title?>" src="../assets/images/<?=$gallery->item_name?>"></a>
				        <div class="text"><?=$gallery->description?></div>
				    </li>
				    <?php } ?>
				</ul>
				<?php } ?>
			</div>
		</div>
	</div>	
</div>

<div class="modal fade" id="modal-video" tabindex="-1" role="dialog" aria-labelledby="modal-video-label" style="display: none;">
	<div class="modal-dialog" role="document">
    	<div class="modal-content">
      		<div class="modal-header">
	        	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
	      	</div>
			<div class="modal-body">
				<div class="embed-responsive embed-responsive-16by9 vid-wrapp"> 
                    <iframe id="vi-iframe" class="embed-responsive-item" src="https://www.youtube.com/embed/K44VUjxJhWs"></iframe> 
                </div>
			</div>
    	</div>
  	</div>
</div>
	
