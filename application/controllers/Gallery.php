<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gallery extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->load->model('Mod_gallery');

		$data['galleries'] = $this->Mod_gallery->get_all_galleries();

		$data_header['title'] = 'Gallery';
		$data_header['navigation'] = 
						'<li><a href="'.site_url('home').'#home">HOME</a></li>
						<li><a href="'.site_url('home').'#about">TENTANG</a></li>
						<li><a href="'.site_url('home').'#work">GALLERY</a></li>';

		$this->load->view('header', $data_header);
		$this->load->view('gallery', $data);
		$this->load->view('footer');
	}

}

/* End of file Gallery.php */
/* Location: ./application/controllers/Gallery.php */