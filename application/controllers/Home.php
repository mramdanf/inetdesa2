<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->load->model('Mod_gallery');

		$data['galleries'] = $this->Mod_gallery->get_all_galleries();
		
		$data_header['title'] = 'Home';
		$data_header['navigation'] = 
						'<li><a href="#home">HOME</a></li>
						<li><a href="#about">TENTANG</a></li>
						<li><a href="#work">GALLERY</a></li>';

		$this->load->view('header', $data_header);

		$this->load->view('home.php', $data);
		$this->load->view('footer.php');
	}

}

/* End of file Home.php */
/* Location: ./application/controllers/Home.php */