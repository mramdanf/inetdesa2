<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gallery extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if ($this->session->userdata(utility::ADM_SES) == NULL) 
		{
			$this->session->set_userdata(utility::TAG_CURR_URL, current_url());
			redirect('admin/auth/login');
		}
		$this->load->model('Mod_gallery');
	}

	public function index()
	{
		$filter = $this->input->get('filter');
		$filter = ($filter == "foto" || $filter == "" || $filter == NULL) ? "1":"2";

		$data = array(
			'page_view' => 'admin/gallery',
			'title' => '',
			'breadcrumb' => '<li>Upload Foto dan Video</li>',
			'type' => $filter,
			'galleries' => $this->Mod_gallery->get_galleries($filter)
			);
		$this->load->view('admin/template', $data);
	}

	function upload()
	{
		$post_data = $this->input->post();

		if ($post_data['type'] == "1") 
		{
			$config['upload_path'] = './assets/images';
			$config['allowed_types'] = 'jpeg|jpg|png';
			$config['max_size']  = '2000';
			$config['encrypt_name'] = TRUE;
			
			$this->load->library('upload', $config);
			
			if ( ! $this->upload->do_upload("img_file"))
			{
				$error = array('error' => $this->upload->display_errors());
				$this->utility->plog($error);
				$this->utility->set_flash(utility::FAILED, 
					"Data gagal disimpan. " . strip_tags($error['error']));
				redirect('admin/gallery');

			} else
			{
				$data = array('upload_data' => $this->upload->data());
				$post_data['item_name'] = $this->upload->data()['file_name'];
				$this->modify($post_data);
			}
		
		} else
		{
			$post_data['y_link'] = $post_data['y_id'];
			$post_data['y_id'] = $this->getYoutubeIdFromUrl($post_data['y_id']);
			$post_data['item_name'] = $post_data['y_id'] . '.jpg';

			// save image thumbnail for video
			$content = file_get_contents("http://img.youtube.com/vi/".$post_data['y_id']."/0.jpg");
			
			//Store in the filesystem.
			$fp = fopen("./assets/images/".$post_data['item_name'], "w");
			fwrite($fp, $content);
			fclose($fp);

			// save videos properties to DB
			$this->modify($post_data);
		}			
	}

	function modify($data = NULL)
	{
		if ($this->Mod_gallery->modify($data)) 
		{
			$this->utility->set_flash(utility::SUCCESS, "Data berhasil disimpan.");

		} else
		{
			$this->utility->set_flash(utility::FAILED, "Terjadi error, data berhasil disimpan.");
			
		}

		redirect('admin/gallery');
	}

	function getYoutubeIdFromUrl($url) 
	{
	    $parts = parse_url($url);
	    
	    if(isset($parts['query']))
	    {
	        parse_str($parts['query'], $qs);
	        if(isset($qs['v']))
	        {
	            return $qs['v'];

	        }else if(isset($qs['vi']))
	        {
	            return $qs['vi'];
	        }
	    }

	    if(isset($parts['path']))
	    {
	        $path = explode('/', trim($parts['path'], '/'));
	        return $path[count($path)-1];
	    }
	    return false;
	}

}

/* End of file Gallery.php */
/* Location: ./application/controllers/admin/Gallery.php */