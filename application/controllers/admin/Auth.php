<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	}

	function login()
	{
		$this->load->view('admin/login');
	}

	function check_user()
	{
		$usern = $this->input->post('username');
		$pass = $this->input->post('password');

		if ($usern == "inetdesa" && $pass == "metrasat") 
		{
			$this->session->set_userdata(utility::ADM_SES, "admin");
			$curr_url = $this->session->userdata(utility::TAG_CURR_URL);
			if ($curr_url != NULL) 
			{
				redirect($curr_url);
			
			} else
			{
				redirect('admin/gallery');
			}
		
		} else 
		{
			$this->utility->set_flash(utility::FAILED, "Username atau password salah.");
			redirect('admin/auth/login');
		}
	}

	function logout()
	{
		$this->session->sess_destroy();
		redirect('admin/auth/login');
	}

}

/* End of file Auth.php */
/* Location: ./application/controllers/admin/Auth.php */