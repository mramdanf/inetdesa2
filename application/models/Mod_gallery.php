<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mod_gallery extends CI_Model {

	private $gallery_tbl = "gallery";

	function __construct()
	{
		parent::__construct();
	}

	function modify($data = NULL)
	{
		if (!isset($data['id'])) 
		{
			return $this->db->insert($this->gallery_tbl, $data);
		}
	}

	function get_galleries($type = "")
	{
		return $this->db->get_where($this->gallery_tbl, array('type'=>$type))->result();
	}

	function get_all_galleries()
	{
		return $this->db->get($this->gallery_tbl)->result();
	}
	

}

/* End of file Mod_gallery.php */
/* Location: ./application/models/Mod_gallery.php */