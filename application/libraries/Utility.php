<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Utility
{
	private $CI;

	const PICKUP = "Pickup";
	const SHIFT_1 = "Shift Satu";
	const SHIFT_2 = "Shift Dua";
	const SUCCESS = "success";
	const FAILED = "failed";
	const ALERT_MSG = "alert_msg";
	const ADM_SES = "adm-ses";
	const TAG_CURR_URL = "curr-url";

	function __construct()
	{
		$this->CI =& get_instance();
	}

	/*
		print array to error log
		@params: array $data array to print
	*/
	function plog($data)
	{
		log_message('error', print_r($data, TRUE));
	}

	/*
		set_flash_data for alert
		flag: indicate type of alert
	*/
	function set_flash($flag = "", $msg)
	{
		if($flag != "")
		{
			if($flag == SELF::SUCCESS)
			{
				$this->CI->session->set_flashdata(
				    SELF::ALERT_MSG,
				    array(
				        'msg'=>$msg,
				        'icon' => 'glyph stroked checkmark',
						'icon2' => 'stroked-checkmark',
				        'type' => 'bg-success'
				    )
				);
			} else
			{
				$this->CI->session->set_flashdata(
				    SELF::ALERT_MSG, 
				    array(
				        'msg'=>$msg,
				        'icon' => 'glyph stroked cancel',
						'icon2' => 'stroked-cancel',
				        'type' => 'bg-danger'
				    )
				);
			}
		}
	}

}