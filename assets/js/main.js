jQuery(document).ready(function($){
	//slicknav
	$('#nav').slicknav();
	//sticky
	 $("header").sticky({topSpacing:0});
    //PieChart For Onepage - START CODE
    $('.chart').easyPieChart({
        barColor: '#fff',
        trackColor: '#535353',
        scaleColor: false,
        easing: 'easeOutBounce',
        scaleLength: 1,
        lineCap: 'round',
        lineWidth: 1, //12
        size: 120, //110
        animate: {
            duration: 3000,
            enabled: true
        },
        onStep: function (from, to, percent) {
            $(this.el).find('.percent').text(Math.round(percent));
        }
    });
	//scrollspy
	$('body').scrollspy({
		target: '#navbar-example',
		offset: 95
	});
	//Smooth Scroll
	var $root = $('html, body');
	$('#nav li a').click(function() {
		var href = $.attr(this, 'href');
		$root.animate({
			scrollTop: $(href).offset().top
		}, 1000, function () {
			window.location.hash = href;
		});
		return false;
	});
	//ScrollUp
	$.scrollUp({
		animation: 'slide', // Fade, slide, none
		scrollSpeed: 1000,
		scrollText: [
		  "<i class='fa fa-chevron-up'></i>"
		]
	});
	//counter
	$('.counter').counterUp({
		delay: 10,
		time: 1000
	});
	//WOW
	wow = new WOW(
		{
		  boxClass:     'wow',      // default
		  animateClass: 'animated', // default
		  offset:       0,          // default
		  mobile:       true,       // default
		  live:         true        // default
		}
	  )
	wow.init();
	//isotope
	$('.menu_item li').click(function(){
	  $(".menu_item li").removeClass("active");
	  $(this).addClass("active");        
		var selector = $(this).attr('data-filter'); 
		$(".gallery_item").isotope({ 
			filter: selector, 
			animationOptions: { 
				duration: 750, 
				easing: 'linear', 
				queue: false, 
			}
		});
	});
	//owlCarousel
	$("#brand_logo").owlCarousel({
		items:6,
		itemsDesktop : [1199,6],
		itemsDesktopSmall : [980,6],
		itemsTablet: [768,4],
		itemsTabletSmall: true,
		itemsMobile : [479,2],
		singleItem : false,
		autoPlay:true,
		stopOnHover:true,
		slideSpeed : 500,
	});

	(function( $ ) {

		//Function to animate slider captions 
		function doAnimations( elems ) {
			//Cache the animationend event in a variable
			var animEndEv = 'webkitAnimationEnd animationend';
			
			elems.each(function () {
				var $this = $(this),
					$animationType = $this.data('animation');
				$this.addClass($animationType).one(animEndEv, function () {
					$this.removeClass($animationType);
				});
			});
		}
		
		//Variables on page load 
		var $myCarousel = $('#image_carousel_1'),
			$firstAnimatingElems = $myCarousel.find('.item:first').find("[data-animation ^= 'animated']");
			
		//Initialize carousel 
		$myCarousel.carousel();
		
		//Animate captions in first slide on page load 
		doAnimations($firstAnimatingElems);
		
		//Pause carousel  
		$myCarousel.carousel('pause');
		
		
		//Other slides to be animated on carousel slide event 
		$myCarousel.on('slide.bs.carousel', function (e) {
			var $animatingElems = $(e.relatedTarget).find("[data-animation ^= 'animated']");
			doAnimations($animatingElems);
		});  
		
	})(jQuery);
});
jQuery(window).load(function(){
	//Isotope 
	$(".gallery_item").isotope({
		itemSelector: '.single_items',
		layoutMode: 'fitRows'
	});

	$(".launch_modal_img").on('click', function(event) {
		event.preventDefault();
		var imgName = this.id;

		if ($(this).hasClass('image')) {
			
			$(".vid-wrapp").hide();
			$("#img_modal").show();
			$("#img_modal").attr('src', 'assets/images/' + imgName);
			

		} else if ($(this).hasClass('video')) {
			$(".vid-wrapp").show();
			$("#img_modal").hide();
			$("#vi-iframe").attr('src', 'https://www.youtube.com/embed/'+imgName);
		}
		$( '#' + $(this).data('target') ).modal();

		
	});

});